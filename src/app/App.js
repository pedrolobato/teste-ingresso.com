import React, { Component } from 'react'
import './App.scss'

import { Header, Content } from '../layout'

class App extends Component {
	render() {
		return (
			<div>
				<Header />
				<Content />
			</div>
		)
	}
}

export default App
