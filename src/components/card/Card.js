import React, { Component } from 'react'

import './styles.scss'

import Tag from '../tag'

class Card extends Component {

	static defaultProps = {
		tags: []
	}

	render() {
		const { title, onClick, tags, ...props } = this.props
		return (
			<div className="card bg-dark text-white" onClick={onClick}>
				<img className="card-img" {...props} alt="" />
				<div className="card-img-overlay">
					{tags.map((tag, index) =>
						<Tag key={index} label={tag} style={tag.includes('Família') ?
							'tag-family' : tag.includes('Cultura') ? 'tag-culture' : ''} />
					)}
					<h5 className="card-title mt-1">{title}</h5>
				</div>
			</div>
		)
	}
}

export default Card
