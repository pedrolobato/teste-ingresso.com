import React, { Component } from 'react'

import './styles.scss'

class Dropdown extends Component {
	
	render(){
		return (
			<div className={`dropdown dropdown--${this.props.open ? "open" : "close"}`}>
				<div className="container">
					<div className={`children children--${this.props.open ? "open" : "close"} pt-5 pb-5`}>
						{this.props.children}
					</div>
				</div>
			</div>
		)
	}
}

export default Dropdown
