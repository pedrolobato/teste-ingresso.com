import React, { Component } from 'react'

import './styles.scss'

class Tag extends Component {

	render() {
		const { label, style } = this.props
		return (
			<span className={`tag ${style || ''}`}>{label}</span>
		)
	}
}

export default Tag
