import React, { Component } from 'react'

import './styles.scss'

class NavItem extends Component {
	
	render(){
		return (
			<li className="nav-item">
				<div className="nav-link" onClick={this.props.onClick}>
					<span className="d-none d-sm-inline">{this.props.label}</span>
					<img src={this.props.icon} height="32" width="32" alt=""/>
				</div>
			</li>
		)
	}
}

export default NavItem
