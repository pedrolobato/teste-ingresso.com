import React, { Component } from 'react'

import './styles.scss'

class Navbar extends Component {
	
	render(){
		return (
			<nav className="navbar navbar-expand navbar-dark bg-header">
				<div className="container justify-content-between">
					<div className="navbar-brand">
						<img src={this.props.brand} width="200" alt=""/>
					</div>
					<div className="collapse navbar-collapse">
						<ul className="navbar-nav">
							{this.props.children}
						</ul>
					</div>
				</div>
			</nav>
		)
	}
}

export default Navbar
