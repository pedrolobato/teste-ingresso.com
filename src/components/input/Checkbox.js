import React, { Component } from 'react'

import './styles.scss'

class Checkbox extends Component {

	render() {
		const { label, ...props } = this.props
		return (
			<div className="custom-control custom-checkbox">
				<input {...props} type="checkbox" className="custom-control-input" />
				<label className="custom-control-label" htmlFor={this.props.value}>{label}</label>
			</div>
		)
	}
}

export default Checkbox
