import React, { Component } from 'react'

import './styles.scss'

class Input extends Component {
	
	render(){
		const { className, ...props } = this.props
		return (
			<input {...props} className={`form-control ${className || ''}`}/>
		)
	}
}

export default Input
