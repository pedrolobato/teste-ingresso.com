import React, { Component } from 'react'

import './styles.scss'

class Select extends Component {

	static defaultProps = {
		options: []
	}

	state = {
		value: '',
	}

	handleChange = (event) => {
		const value = event.target.value
		this.setState({ value })
		if (this.props.onChange) this.props.onChange(value)
	}

	render() {
		const { className, options, ...props } = this.props
		return (
			<select {...props} className={`form-control ${className || ''}`} value={this.state.value || this.props.value} onChange={this.handleChange}>
				{options.map((option, index) => (
					<option key={index} value={option.value}>{option.name}</option>
				))}
			</select>
		)
	}
}

export default Select
