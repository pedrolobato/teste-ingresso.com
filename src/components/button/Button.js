import React, { Component } from 'react'

import './styles.scss'

class Button extends Component {
	
	render(){
		const { styleName, className, children, ...props } = this.props
		return (
			<button {...props} className={`btn btn-outline-${styleName || 'primary'} ${className || ''}`}>
				{children}
			</button>
		)
	}
}

export default Button
