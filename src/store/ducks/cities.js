import axios from './config/axios'

const initialState = []

const Types = {
	GET: 'cities/GET'
}

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case Types.GET:
			return payload
		default:
			return state
	}
}

export const getCities = () => async dispatch => {
	try {
		const cities = await axios.get('/states')
		dispatch({ type: Types.GET, payload: cities.data })
	} catch (error) {
		console.log('Error on get cities:', error)
	}
}
