import axios from './config/axios'

const initialState = []

const Types = {
	GET: 'highlights/GET'
}

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case Types.GET:
			return payload
		default:
			return state
	}
}

export const getHighlights = (cityId = '1') => async dispatch => {
	try {
		const highlights = await axios.get(`/templates/highlights/${cityId}/partnership/home`)
		dispatch({ type: Types.GET, payload: highlights.data })
	} catch (error) {
		console.log('Error on get highlights:', error)
	}
}
