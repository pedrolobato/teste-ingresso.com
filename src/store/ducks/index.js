import { combineReducers } from 'redux'

import cities from './cities'
import highlights from './highlights'

export default combineReducers({ cities, highlights })
