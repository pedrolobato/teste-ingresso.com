import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import './styles.scss'

import { Checkbox } from '../../components/input'
import Card from '../../components/card'

import { getHighlights } from '../../store/ducks/highlights'

class Content extends Component {

	state = {
		filters: ['2D', 'DUB', '3D', 'D-BOX', 'LEG', 'XD', 'VIP', '4D', 'Cinépic'],
		filtersSelected: [],
		moviesFilter: [],
	}

	componentDidMount() {
		this.props.getHighlights()
	}

	handleChange = (event) => {
		const checked = event.target.checked
		const currentValue = event.target.value

		let filtersSelected = this.state.filtersSelected

		if (checked) {
			filtersSelected.push(currentValue)
		} else {
			filtersSelected = filtersSelected.filter(value => value !== currentValue)
		}

		this.setState({ filtersSelected })

		this.filterMovies(filtersSelected)
	}

	onClickBanner = ({ event }) => {
		const trailers = event.trailers
		window.open(trailers.length ? trailers[0].url : event.siteURL, '_blank')
	}

	filterMovies = (filtersSelected) => {
		if (filtersSelected.length) {
			const moviesFilter = this.props.highlights.filter(highlight => {
				return highlight.showtimes.find(showtime => {
					return showtime.rooms.find(room => {
						return room.sessions.find(session => {
							return session.types.find(type => {
								return filtersSelected.includes(type.alias)
							})
						})
					})
				})
			})

			this.setState({ moviesFilter })
		}
	}

	renderMovies = (highlights) => {
		return (
			highlights.map((highlight, index) =>
				<div key={index} className='col-sm col-md-4 col-lg-3 mb-5'>
					<Card
						title={highlight.event.title}
						src={highlight.event.images[0].url}
						tags={highlight.event.tags}
						onClick={() => this.onClickBanner(highlight)}
					/>
				</div>
			)
		)
	}

	render() {
		if (this.props.highlights.length)
			return (
				<div className="container">
					<div className="content">
						<div className="row mb-3">
							<div className="col">
								<h5>Filmes</h5>
								<div className="row">
									{this.state.filters.map((value, index) =>
										<div key={index} className="col-4 col-sm-3 col-md-2">
											<Checkbox id={value} label={value} value={value} onChange={this.handleChange} />
										</div>
									)}
								</div>
							</div>
						</div>

						<div className="row">
							<div className="col">
								<h6 className="title">EM CARTAZ</h6>
								<hr />
								<div className="row mt-4">
									{this.renderMovies(
										this.state.filtersSelected.length ?
											this.state.moviesFilter :
											this.props.highlights
									)}
								</div>
							</div>
						</div>
					</div>
				</div>
			)
		else return <div className="loading"></div>
	}
}

const mapStateToProps = state => ({ highlights: state.highlights })
const mapDipatchToProps = dispatch => bindActionCreators({ getHighlights }, dispatch)

export default connect(mapStateToProps, mapDipatchToProps)(Content)
