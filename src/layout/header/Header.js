import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import './styles.scss'
import logo from '../../images/logo.svg'
import search from '../../images/search.svg'
import place from '../../images/place.svg'

import Navbar, { NavItem } from '../../components/navbar'
import Tag from '../../components/tag'
import Dropdown from '../../components/dropdown'
import Input from '../../components/input'

import { getCities } from '../../store/ducks/cities'
import { getHighlights } from '../../store/ducks/highlights'

class Header extends Component {

	state = {
		openSearch: false,
		openPlace: false,
		citySelected: {},
		citiesFilter: [],
		moviesFilter: [],
		sizeValueFilterCities: '',
		sizeValueFilterMovies: '',
		cityDefault: '441'
	}

	componentDidMount() {
		this.props.getCities()
	}

	componentDidUpdate(props, state) {
		if (!this.state.citySelected.id) {
			this.props.cities.forEach(state => {
				state.cities.forEach(city => {
					if (city.id === this.state.cityDefault) {
						this.selectCity(city)
					}
				})
			})
		}
	}

	selectCity = (city) => {
		this.setState({ openPlace: false, citySelected: city })
		this.props.getHighlights(city.id)
	}

	filterCity = (event) => {
		const text = event.target.value
		this.setState({ sizeValueFilterCities: text.length })
		console.log(text)
		if (text.length >= 2) {
			const citiesFilter = this.props.cities.map(state => {
				const cities = state.cities.filter(city =>
					city.urlKey.includes(text.toLowerCase()) ||
					city.name.toLowerCase().includes(text.toLowerCase())
				)
				return { ...state, cities }
			})
			this.setState({ citiesFilter })
		}
	}

	filterMovies = (event) => {
		const text = event.target.value
		this.setState({ sizeValueFilterMovies: text.length })
		console.log(text)
		if (text.length >= 2) {
			const moviesFilter = this.props.highlights.filter(highlight => {
				return highlight.event.urlKey.includes(text.toLowerCase()) ||
					highlight.event.title.toLowerCase().includes(text.toLowerCase())
			})
			this.setState({ moviesFilter })
		}
	}

	renderListCities = (data) => {
		return (
			data.map(state =>
				state.cities.length ?
					<div key={state.uf}>
						<p className="state-name">{state.uf} - {state.name}</p>
						{state.cities.map(city =>
							<div key={city.id} className="city-name" onClick={() => { this.selectCity(city) }}>
								{city.name}
							</div>
						)}
					</div>
					: null
			))
	}

	renderListMovies = (data) => {
		return (
			data.map(highlight =>
				<div key={highlight.event.id} className="movie row mt-4" onClick={() => this.onClickBanner(highlight)}>
					<div className="col-4">
						<img className="img-fluid" src={highlight.event.images[0].url} alt="" />
					</div>
					<div className="col-8">
						<h6>{highlight.event.title}</h6>
						{highlight.event.tags.map((tag, index) =>
							<Tag key={index} label={tag} style={tag.includes('Família') ?
								'tag-family' : tag.includes('Cultura') ? 'tag-culture' : ''} />
						)}
					</div>
				</div>
			))
	}

	onClickBanner = ({ event }) => {
		const trailers = event.trailers
		window.open(trailers.length ? trailers[0].url : event.siteURL, '_blank')
	}

	render() {
		return (
			<div className="fixed-top">
				<Navbar brand={logo}>
					<NavItem label='Busca' icon={search}
						onClick={() => this.setState(state => ({ openSearch: !state.openSearch, openPlace: false }))} />
					<NavItem label={this.state.citySelected.name} icon={place}
						onClick={() => this.setState(state => ({ openPlace: !state.openPlace, openSearch: false }))} />
				</Navbar>

				<Dropdown open={this.state.openSearch}>
					<p>O que você procura?</p>
					<Input placeholder='Pesquise por filmes...' onKeyUp={this.filterMovies} />
					{this.renderListMovies(
						this.state.sizeValueFilterMovies ?
							this.state.moviesFilter :
							this.props.highlights
					)}
				</Dropdown>

				<Dropdown open={this.state.openPlace}>
					<p>Você está em <b>{this.state.citySelected.name}</b></p>
					<Input placeholder='Buscar cidade...' onKeyUp={this.filterCity} />
					{this.renderListCities(
						this.state.sizeValueFilterCities ?
							this.state.citiesFilter :
							this.props.cities
					)}
				</Dropdown>
			</div>
		)
	}
}

const mapStateToProps = state => ({ cities: state.cities, highlights: state.highlights })
const mapDipatchToProps = dispatch => bindActionCreators({ getCities, getHighlights }, dispatch)

export default connect(mapStateToProps, mapDipatchToProps)(Header)
